﻿using System.Linq.Expressions;
using Zerox.Core.Common.Cache;

namespace Zerox.Cache
{
    public interface ICacheService
    {
        // set key value pair into cahce consider that value can be any thing, we will do json then
        void Set<T>(string key, T value, int ttl = 60);

        Task SetAsync<T>(string key, T value, int ttl = 60);

        // generic version of Get
        T? Get<T>(string key);

        Task<T?> GetAsync<T>(string key);

        // delete value from cache by key
        void Delete(string key);

        Task DeleteAsync(string key);

        // key is composed string with . separated namespacing, we can delete whoile name space of variables
        void DeleteByNamespace(string key);

        Task DeleteByNamespaceAsync(string key);
    }

    public interface IIndexedCacheService<EntT> : ICacheService
        where EntT : ICachedEntity
    {
        /// <summary>
        /// This will save entity in cache doing all necesary indexes
        /// </summary>
        /// <param name="entity"></param>
        void Save(EntT entity, int ttl = 60);

        /// <summary>
        /// This will save entity in cache doing all necesary indexes async
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task SaveAsync(EntT entity, int ttl = 60);

        /// <summary>
        /// Get entity from cache by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        EntT? Load(string key);

        /// <summary>
        /// Get entity from cache by key async
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        Task<EntT?> LoadAsync(string key);

        /// <summary>
        /// Get entity from cache using specific index name and its value
        /// </summary>
        /// <param name="indexName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        EntT? LoadByIndex(string indexName, string value);

        /// <summary>
        /// Get entity from cache using specific index name and its value
        /// </summary>
        /// <param name="indexName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        Task<EntT?> LoadByIndexAsync(string indexName, string value);

        /// <summary>
        /// Get entity from cache using specific index property and its value
        /// </summary>
        /// <param name="index"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        EntT? LoadByIndex<TProp>(Expression<Func<EntT, TProp>> index, string value);

        /// <summary>
        /// Get entity from cache using specific index property and its value
        /// </summary>
        /// <param name="index"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        Task<EntT?> LoadByIndexAsync<TProp>(Expression<Func<EntT, TProp>> index, string value);

        /// <summary>
        /// delete entity from cache
        /// </summary>
        /// <param name="entity"></param>
        void Delete(ICachedEntity entity);

        /// <summary>
        /// delete entity from cache
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task DeleteAsync(ICachedEntity entity);

    }
}
