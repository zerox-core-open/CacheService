﻿using Zerox.Cache;
using Zerox.Cache.Model;
using Zerox.Core.Common.Cache;

namespace Zerox.Cahce.Service
{

    public class CacheService
        : ICacheService, IDisposable 
       
    {
        private readonly ICacheBackend _backend;

        public CacheService(ICacheBackend backend)
        {
            _backend = backend;
        }

        public bool disposedValue;

        public void Delete(string key)
        {
            _backend.Delete(key);
        }

        public void DeleteByNamespace(string key)
        {
            _backend.DeleteByNamespace(key);
        }

        public virtual T? Get<T>(string key)
        {
            return _backend.Get<T>(key);
        }

        public void Set<T>(string key, T value, int ttl)
        {
            _backend.Set(key, value, ttl);
        }

        public async Task SetAsync<T>(string key, T value, int ttl)
        {
            await _backend.SetAsync<T>(key, value, ttl);
        }

        public async Task<T?> GetAsync<T>(string key)
        {
            var value = await _backend.GetAsync<T>(key);
            return value;
        }

        public async Task DeleteAsync(string key)
        {
            await _backend.DeleteAsync(key);
        }

        public async Task DeleteByNamespaceAsync(string key)
        {
            await _backend.DeleteByNamespaceAsync(key);
        }


        #region dispose

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (_backend is IDisposable disposableBackend)
                    {
                        disposableBackend.Dispose();
                    }
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~CacheService()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }

    public class IndexedCahceService<EntT> : CacheService, IIndexedCacheService<EntT>
        where EntT : ICachedEntity
    {
        private string _namespace = typeof(EntT).Name;

        private string MainIxValue(string val)  => _namespace + "." + val;
        private string SecIxValue(string sec, string val) => _namespace + "." + sec + "." + val;

        public IndexedCahceService(ICacheBackend backend) : base(backend)
        {
            // do mapping
            CacheModel.Map<EntT>();
        }

        public void Delete(ICachedEntity entity)
        {
            // get mapping of indexes
            var mapping = entity.GetMapping();
            // get all indexes
            var indexes = mapping.Properties;
            // check if there is main doing where over map values
            var main = indexes.Values.Where(x => x.Ix?.IsMain ?? false).FirstOrDefault();
            if (main == null)
            {
                // we will take first index property found
                main = indexes.Values.FirstOrDefault();
            }

            if (main == null)
            {
                throw new ApplicationException("No main index found!");
            }

            // get main index value
            var mainIndexValue = main.PropertyInfo.GetValue(entity)?.ToString();
            if (mainIndexValue == null)
            {
                throw new ApplicationException("Main index value is null!");
            }

            foreach (var ip in indexes.Values)
            {
                if (ip == main)
                {
                    // do real cluster index
                    Delete(MainIxValue(mainIndexValue));
                }
                else
                {
                    // do secondary indexes
                    var secIndexValue = ip.PropertyInfo.GetValue(entity)?.ToString();
                    if (secIndexValue != null)
                    {
                        Delete(SecIxValue(ip.PropertyInfo.Name, secIndexValue));
                    }
                }

            }
        }

        public Task DeleteAsync(ICachedEntity entity)
        {
            return Task.Run(() => Delete(entity));
        }

        public EntT? Load(string key)
        {
            var e = base.Get<EntT>(MainIxValue(key));
            if (e is EntT en) return en;
            return default;
        }

        public async Task<EntT?> LoadAsync(string key)
        {
            var e = await GetAsync<EntT>(MainIxValue(key));
            if (e is EntT en) return en;
            return default;
        }

        public EntT? LoadByIndex(string indexName, string value)
        {
            var e = Get<string>(SecIxValue(indexName, value));
            if (e is string en) return Load(en);
            return default;
        }

        public Task<EntT?> LoadByIndexAsync(string indexName, string value)
        {
            return Task.Run(() => LoadByIndex(indexName, value));
        }

       

        public void Save(EntT entity, int ttl)
        {
            // get mapping of indexes
            var mapping = entity.GetMapping();
            // get all indexes
            var indexes = mapping.Properties;
            // check if there is main doing where over map values
            var main = indexes.Values.Where(x => x.Ix?.IsMain ?? false).FirstOrDefault();
            if(main == null)
            {   
                // we will take first index property found
                main = indexes.Values.FirstOrDefault();
            }

            if(main == null)
            {
                throw new ApplicationException("No main index found!");
            }

            // get main index value
            var mainIndexValue = main.PropertyInfo.GetValue(entity)?.ToString();
            if (mainIndexValue == null)
            {
                throw new ApplicationException("Main index value is null!");
            }
            
            foreach (var ip in indexes.Values)
            {
                if(ip == main)
                {
                    // do real cluster index
                    Set(MainIxValue(mainIndexValue), entity, ttl);
                } else
                {
                    // do secondary indexes
                    var secIndexValue = ip.PropertyInfo.GetValue(entity)?.ToString();
                    if (secIndexValue != null)
                    {
                        Set(SecIxValue(ip.PropertyInfo.Name, secIndexValue), mainIndexValue, ttl);
                    }
                }

            }
        }

        public Task SaveAsync(EntT entity, int ttl)
        {
            return Task.Run(() => Save(entity, ttl));
        }

        public EntT? LoadByIndex<TProp>(System.Linq.Expressions.Expression<Func<EntT, TProp>> index, string value)
        {
            var ixName = index.GetPropertyInfo().Name;
            return LoadByIndex(ixName, value);
        }

        public Task<EntT?> LoadByIndexAsync<TProp>(System.Linq.Expressions.Expression<Func<EntT, TProp>> index, string value)
        {
            var ixName = index.GetPropertyInfo().Name;
            return LoadByIndexAsync(ixName, value);
        }
    }
}
