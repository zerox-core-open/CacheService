﻿using System.Collections.Concurrent;
using System.Globalization;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using Zerox.Core.Common.Cache;
using Zerox.Core.Support;

namespace Zerox.Cache.Model
{
    public record CacheIndexPropertyDescriptor(PropertyInfo PropertyInfo, IndexAttribute? Ix);


    public interface IChacheEntityMapping
    {
        Dictionary<string, CacheIndexPropertyDescriptor> Properties { get; }
    }

    public class ChacheEntityMapping<EntT> : IChacheEntityMapping
        where EntT : ICachedEntity
    {
        private bool _builded = false;
        /// <summary>
        /// properties cache
        /// </summary>
        private Dictionary<string, CacheIndexPropertyDescriptor>? _properties;
        // getter
        public Dictionary<string, CacheIndexPropertyDescriptor> Properties => _properties ?? throw new ApplicationException("Mapping not build!");


        public static ChacheEntityMapping<EntT> Create()
        {
            return new ChacheEntityMapping<EntT>();
        }

        public ChacheEntityMapping<EntT> WithDefaultPropertiesMapping()
        {
            if (_builded) return this;

            _properties = new Dictionary<string, CacheIndexPropertyDescriptor>();
            var props = typeof(EntT).GetAttributes<IndexAttribute>();
            foreach (var prop in props)
            {
                _properties.Add(prop.Item1.Name, new CacheIndexPropertyDescriptor(prop.Item1, prop.Item2));
            }
            return this;
        }


        // build
        public ChacheEntityMapping<EntT> Build()
        {
            _builded = true;
            return this;
        }

    }

    /// <summary>
    ///  global model cache
    /// </summary>
    public static class CacheModel
    {
        private static readonly ConcurrentDictionary<Type, IChacheEntityMapping> _mappingsCahce = new ConcurrentDictionary<Type, IChacheEntityMapping>();

        public static PropertyInfo GetPropertyInfo<T, U>(this Expression<Func<T, U>> expression)
        {
            if (expression.Body is MemberExpression member)
            {
                return (PropertyInfo)member.Member;
            }
            else if (expression.Body is UnaryExpression unaryExpression)
            {
                // If the property is of a value type, the body will be a UnaryExpression
                member = (MemberExpression)unaryExpression.Operand;
                return (PropertyInfo)member.Member;
            }

            throw new ArgumentException("Invalid expression");
        }

        public static IChacheEntityMapping GetMapping<EntT>(this EntT src)
            where EntT : ICachedEntity
        {
            if (_mappingsCahce.TryGetValue(src.GetType(), out IChacheEntityMapping? m))
            {
                return m;
            }
            else
            {
                return Map<EntT>();
            }
        }

        public static IChacheEntityMapping? GetMapping(this Type type)
        {
            if (_mappingsCahce.TryGetValue(type, out IChacheEntityMapping? m))
            {
                return m;
            }
            else
            {
                return (IChacheEntityMapping?)typeof(CacheModel).GetMethods().Where(m => m.Name == "Map" && !m.GetParameters().Any()).FirstOrDefault()?.MakeGenericMethod(type).Invoke(null, null);
            }
        }



        public static void Map<EntT>(Action<ChacheEntityMapping<EntT>> action)
            where EntT : ICachedEntity
        {
            var mapping = ChacheEntityMapping<EntT>.Create();
            action(mapping);
            _mappingsCahce.TryAdd(typeof(EntT), mapping.Build());
        }

        public static IChacheEntityMapping Map<EntT>()
            where EntT : ICachedEntity
        {
            var mapping = ChacheEntityMapping<EntT>.Create()
                .WithDefaultPropertiesMapping()
                .Build();

            _mappingsCahce.TryAdd(typeof(EntT), mapping);

            return mapping;
        }
    }
}
