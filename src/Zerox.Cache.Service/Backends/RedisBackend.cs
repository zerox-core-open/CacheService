﻿using MessagePack;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using StackExchange.Redis;

namespace Zerox.Cache.Backends
{
    public class RedisBackendConfig
    {
        public int TtlInterval { get; set; } = 10;

    }

    [MessagePackObject]
    public struct RedisCacheValue<T>
    {
        [Key("value")]
        public readonly T value;
        [Key("ttls")]
        public readonly int ttls;
        [Key("ttl")]
        public readonly DateTime ttl;

        public RedisCacheValue(T value, int ttls, DateTime ttl)
        {
            this.value = value;
            this.ttls = ttls;
            this.ttl = ttl;
        }
    };

    public class RedisBackend : ICacheBackend
    {
        private readonly IDatabase _cache;
        private readonly ILogger<RedisBackend> _logger;

        public RedisBackend(IDatabase cache, ILogger<RedisBackend> logger)
        {
            _cache = cache;
            _logger = logger;
        }

        public void Delete(string key)
        {
            _cache.KeyDelete(key);
        }

        public async Task DeleteAsync(string key)
        {
            try
            {
                await _cache.KeyDeleteAsync(key);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "Error occurred while removing data with key {Key} from cache", key);
            }
        }

        public void DeleteByNamespace(string key)
        {
            throw new NotImplementedException();
        }

        public Task DeleteByNamespaceAsync(string key)
        {
            throw new NotImplementedException();
        }

        public T? Get<T>(string key)
        {
            try
            {
                var value = _cache.StringGet(key);
                if(!string.IsNullOrEmpty(value))
                {
                    var deserializedValue = MessagePackSerializer.Deserialize<RedisCacheValue<T>>(value);
                    return deserializedValue.value;
                }
                return default;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while getting key {Key} from cache", key);
                return default;
            }

        }


        public async Task<T?> GetAsync<T>(string key)
        {
            try
            {
                var value = await _cache.StringGetAsync(key);
                if(!string.IsNullOrEmpty(value))
                {
                    var deserializedValue = MessagePackSerializer.Deserialize<RedisCacheValue<T>>(value);
                    return deserializedValue.value;
                }
                return default;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while getting key {Key} from cache", key);
                return default;
            }
        }

        public void Set<T>(string key, T value, int ttl = 60)
        {
            try
            {
                RedisCacheValue<T> val = new RedisCacheValue<T>(value, ttl, DateTime.Now + TimeSpan.FromSeconds(ttl));
                var serializedValue = MessagePackSerializer.Serialize(val);
                _cache.StringSet(key, serializedValue, TimeSpan.FromSeconds(ttl));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while setting key {Key} in cache", key);
            }
        }


        public async Task SetAsync<T>(string key, T value, int ttl = 60)
        {
            try
            {
                RedisCacheValue<T> val = new RedisCacheValue<T>(value, ttl, DateTime.Now + TimeSpan.FromSeconds(ttl));
                var serializedValue = MessagePackSerializer.Serialize(val);
                await _cache.StringSetAsync(key, serializedValue, TimeSpan.FromSeconds(ttl));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while setting key {Key} in cache", key);
            }
        }
    }
}
