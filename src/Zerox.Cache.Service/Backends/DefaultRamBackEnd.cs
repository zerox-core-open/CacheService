﻿using Microsoft.Extensions.Options;
using System.Collections.Concurrent;

namespace Zerox.Cache.Backends
{
    public class DefaultRamBackEndConfig
    {
        public int TtlInterval { get; set; } = 10;

    }

    internal record CacheValue(object value, int ttls, DateTime ttl);
    public class DefaultRamBackEnd : ICacheBackend
    {
        private static ConcurrentDictionary<string, CacheValue> _cache = new ConcurrentDictionary<string, CacheValue>();

        private DefaultRamBackEndConfig _cfg;

        private System.Timers.Timer _timer;

        public DefaultRamBackEnd(IOptions<DefaultRamBackEndConfig> cfg)
        {
            _cfg = cfg?.Value ?? new DefaultRamBackEndConfig();
            _timer = new System.Timers.Timer(TimeSpan.FromSeconds(_cfg.TtlInterval).TotalMilliseconds);
            _timer.Elapsed += On_timer_Elapsed;
            _timer.Start();
            
        }

        private void On_timer_Elapsed(object? sender, System.Timers.ElapsedEventArgs e)
        {
            //DateTime now = DateTime.Now;
            if(_cache.Count != 0)
            {
                foreach (var entry in _cache)
                {
                    if (entry.Value.ttl <= e.SignalTime)
                    {
                        // Entry has expired, remove it from the cache
                        _cache.TryRemove(entry.Key, out _);
                    }
                }
            } 
        }


        public void Delete(string key)
        {
            _cache.TryRemove(key, out _);
        }

        public void DeleteByNamespace(string key)
        {
            foreach (var cacheKey in _cache.Keys.ToArray())
            {
                if (cacheKey.StartsWith(key + "."))
                {
                    _cache.TryRemove(cacheKey, out _);
                }
            }
        }

        public T? Get<T>(string key)
        {
            if (_cache.TryGetValue(key, out var value) && value is CacheValue tv)
            {
                return (T?)tv.value;
            }

            return default;
        }

        

        public void Set<T>(string key, T value, int ttl = 60)
        {
                _cache.TryAdd(key, new CacheValue(value, ttl, DateTime.Now + TimeSpan.FromSeconds(ttl)));
        }

        public void Dispose()
        {
            _cache.Clear();
        }

        public Task SetAsync<T>(string key, T value, int ttl = 60)
        {
            return Task.Run(() => Set(key, value));
        }

        public Task<T?> GetAsync<T>(string key)
        {
            return Task.FromResult(Get<T>(key));
        }

        public Task DeleteAsync(string key)
        {
            return Task.Run(() => Delete(key));
        }

        public Task DeleteByNamespaceAsync(string key)
        {
            return Task.Run(() => DeleteByNamespace(key));
        }
    }
}
