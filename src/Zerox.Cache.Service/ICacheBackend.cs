﻿namespace Zerox.Cache
{
    // backend for chache service
    public interface ICacheBackend
    {
        // set key value pair into cahce consider that value can be any thing, we will do json then
        void Set<T>(string key, T value, int ttl = 60);

        Task SetAsync<T>(string key, T value, int ttl = 60);

        // generic version of Get
        T? Get<T>(string key);

        Task<T?> GetAsync<T>(string key);

        // delete value from cache by key
        void Delete(string key);

        Task DeleteAsync(string key);

        // key is composed string with . separated namespacing, we can delete whoile name space of variables
        void DeleteByNamespace(string key);

        Task DeleteByNamespaceAsync(string key);
    }
}
