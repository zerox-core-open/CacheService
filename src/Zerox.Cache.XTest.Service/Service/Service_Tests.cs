using Zerox.Cache.Fixture;
using Microsoft.Extensions.Options;

namespace Zerox.Cache.Service
{
    public class Service_Tests : TestBed<TestFixture>
    {
        private readonly Options _options;

        public Service_Tests(ITestOutputHelper testOutputHelper, TestFixture fixture) : base(testOutputHelper, fixture)
        {
            _options = _fixture.GetService<IOptions<Options>>(_testOutputHelper)!.Value;
        }

        [Fact]
        public void SetAndGet_WithValidKeyAndValue_ShouldRetrieveCorrectValue()
        {
            // Arrange
            var cacheService = _fixture.GetService<ICacheService>(_testOutputHelper);
            Assert.NotNull(cacheService);
            // Act
            cacheService.Set("key1", "value1");
            var retrievedValue = cacheService.Get<object>("key1");

            // Assert
            Assert.Equal("value1", retrievedValue);
        }

        [Fact]
        public async Task SetAndGetAsync_WithValidKeyAndValue_ShouldRetrieveCorrectValue()
        {
            // Arrange
            var cacheService = _fixture.GetService<ICacheService>(_testOutputHelper);
            Assert.NotNull(cacheService);
            // Act
            await cacheService.SetAsync("key1", "value1");
            var retrievedValue = await cacheService.GetAsync<object>("key1");

            Assert.Equal("value1", retrievedValue);
        }

        [Fact]
        public void Delete_WithExistingKey_ShouldRemoveValue()
        {
            // Arrange
            var cacheService = _fixture.GetService<ICacheService>(_testOutputHelper);
            Assert.NotNull(cacheService);
            cacheService.Set("key1", "value1");

            // Act
            cacheService.Delete("key1");
            var retrievedValue = cacheService.Get<string>("key1");

            // Assert
            Assert.Null(retrievedValue);
        }

        //[Fact]
        //public void DeleteByNamespace_WithExistingKeysInNamespace_ShouldRemoveValues()
        //{
        //    // Arrange
        //    var cacheService = _fixture.GetService<ICacheService>(_testOutputHelper);
        //    Assert.NotNull(cacheService);
        //    cacheService.Set("namespace.key1", "value1");
        //    cacheService.Set("namespace.key2", "value2");

        //    // Act
        //    cacheService.DeleteByNamespace("namespace");
        //    var retrievedValue1 = cacheService.Get<string>("namespace.key1");
        //    var retrievedValue2 = cacheService.Get<string>("namespace.key2");

        //    // Assert
        //    Assert.Null(retrievedValue1);
        //    Assert.Null(retrievedValue2);
        //}

        //[Fact]
        //public void Dispose_WithDisposableBackend_ShouldDisposeBackend()
        //{
        //    // Arrange
        //    var cacheService = _fixture.GetService<ICacheService>(_testOutputHelper);
        //    Assert.NotNull(cacheService);

        //    // Act
        //    cacheService.Dispose();

        //    // Assert
        //    Assert.True(cacheService.disposedValue);
        //}

        [Fact]
        public void SetAndGetObject_WithValidKeyAndValue_ShouldRetrieveCorrectValue()
        {
            // Arrange
            var cacheService = _fixture.GetService<ICacheService>(_testOutputHelper);
            Assert.NotNull(cacheService);

            // Act
            cacheService.Set<object>("key1", new { Name = "John", Age = 30 });
            var retrievedValue = cacheService.Get<Dictionary<string, object>>("key1");
            var actualAge = Convert.ToInt32(retrievedValue["Age"]);
            // Assert
            Assert.Equal("John", retrievedValue["Name"]);
            Assert.Equal(30, actualAge);
        }

        [Fact]
        public async Task MultiThreadedSetAndGet_ShouldBeThreadSafe()
        {
            // Arrange
            var cacheService = _fixture.GetService<ICacheService>(_testOutputHelper);
            Assert.NotNull(cacheService);
            var tasks = new List<Task>();

            // make array of 1000 tuples with item1 index and item2 random value
            var values = Enumerable.Range(0, 1000).Select(i => (i, Guid.NewGuid().ToString())).ToArray();

            for(int i = 0; i < 500; ++i)
            {
                foreach(var t in values)
                {
                    tasks.Add(Task.Run(async () =>
                    {
                        await cacheService.SetAsync($"{t.Item1}", t.Item2);
                    }));
                }
            }

            await Task.WhenAll(tasks);

            // assert all in chache is correct
            foreach(var t in values)
            {
                var retrievedValue = await cacheService.GetAsync<string>($"{t.Item1}");
                Assert.Equal(t.Item2, retrievedValue);
            }
           
        }

        [Fact]
        public async Task MultiThreadedDelete_ShouldBeThreadSafe()
        {
            // Arrange
            var cacheService = _fixture.GetService<ICacheService>(_testOutputHelper);
            Assert.NotNull(cacheService);
            await cacheService.SetAsync("key1", "value1");

            var tasks = new Task[10];

            // Act
            for (int i = 0; i < tasks.Length; i++)
            {
                tasks[i] = Task.Run(async () =>
                {
                    await cacheService.DeleteAsync("key1");
                });
            }

            await Task.WhenAll(tasks);

            var retrievedValue = await cacheService.GetAsync<object>("key1");

            // Assert
            Assert.Null(retrievedValue);
        }

        //[Fact]
        //public async Task MultiThreadedDeleteByNamespace_ShouldBeThreadSafe()
        //{
        //    // Arrange
        //    var cacheService = _fixture.GetService<ICacheService>(_testOutputHelper);
        //    Assert.NotNull(cacheService);
        //    await cacheService.SetAsync("namespace.key1", "value1");
        //    await cacheService.SetAsync("namespace.key2", "value2");

        //    var tasks = new Task[10];

        //    // Act
        //    for (int i = 0; i < tasks.Length; i++)
        //    {
        //        tasks[i] = Task.Run(async () =>
        //        {
        //            await cacheService.DeleteByNamespaceAsync("namespace");
        //        });
        //    }

        //    await Task.WhenAll(tasks);

        //    var retrievedValue1 = await cacheService.GetAsync<object>("namespace.key1");
        //    var retrievedValue2 = await cacheService.GetAsync<object>("namespace.key2");

        //    // Assert
        //    Assert.Null(retrievedValue1);
        //    Assert.Null(retrievedValue2);
        //}

        [Fact]
        public async Task ExpiredCacheEntries_ShouldBeRemoved()
        {
            // Arrange
            var cacheService = _fixture.GetService<ICacheService>(_testOutputHelper);
            Assert.NotNull(cacheService);

            // Act
            cacheService.Set<string>("key1", "value1", 1);
            await Task.Delay(TimeSpan.FromSeconds(2)); // Wait for expiration
            var retrievedValue = cacheService.Get<string>("key1");

            // Assert
            Assert.Null(retrievedValue);
        }

        [Fact]
        public async Task UpdatesTTL_Successfully()
        {
            // Arrange
            var cacheService = _fixture.GetService<ICacheService>(_testOutputHelper);
            Assert.NotNull(cacheService);

            // Act
            cacheService.Set<string>("key1", "value1", 1);
            await Task.Delay(TimeSpan.FromMilliseconds(500)); // Wait for half the TTL
            var retrievedValue = cacheService.Get<string>("key1");

            // Assert
            Assert.Equal("value1", retrievedValue);

            // Act
            await Task.Delay(TimeSpan.FromMilliseconds(700)); // Wait for the rest of TTL
            retrievedValue = cacheService.Get<string>("key1");

            // Assert
            Assert.Null(retrievedValue);
        }
    }
}