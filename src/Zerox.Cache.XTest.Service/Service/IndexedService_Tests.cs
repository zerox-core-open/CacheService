using Zerox.Cache.Backends;
using Zerox.Cahce.Service;
using Zerox.Cache;
using Zerox.Cache.Fixture;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using MessagePack;
using Zerox.Core.Support;
using Zerox.Core.Common.Cache;

namespace Zerox.Cache.Service
{
    [MessagePackObject]
    public class TestChachedEntity : ICachedEntity
    {
        [Key(0)]
        [Index(IsMain = true)]
        public string Name { get; set; } = string.Empty;

        [Key(1)]
        [Index]
        public int Age { get; set; }

        [Key(2)]
        public string Data { get; set; } = string.Empty;

        [Key(3)]
        [Index]
        public string Street { get; set; } = string.Empty;

        [Index]
        [IgnoreMember] // not saved in cache
        public string Md5Street
        {
            get => Street.ToMD5();
        }
    }

    public class IndexedService_Tests : TestBed<TestFixture>
    {
        private readonly Options _options;

       
        public IndexedService_Tests(ITestOutputHelper testOutputHelper, TestFixture fixture) : base(testOutputHelper, fixture)
        {
            _options = _fixture.GetService<IOptions<Options>>(_testOutputHelper)!.Value;
        }

        // add tests for IIndexedCacheService<TestChachedEntity>
        [Fact]
        public void SetAndGet_WithValidKeyAndValue_ShouldRetrieveCorrectValue()
        {
            // Arrange
            var cacheService = _fixture.GetService<IIndexedCacheService<TestChachedEntity>>(_testOutputHelper);
            Assert.NotNull(cacheService);
            // Act
            cacheService.Save(new TestChachedEntity { Name = "name1", Age = 10, Data = "data1", Street = "street1" });
            var retrievedValue = cacheService.Load("name1");

            // Assert
            Assert.NotNull(retrievedValue);
            Assert.Equal("name1", retrievedValue.Name);
            Assert.Equal(10, retrievedValue.Age);
            Assert.Equal("data1", retrievedValue.Data);
            Assert.Equal("street1", retrievedValue.Street);

            // test also the index
            var retrievedValue2 = cacheService.LoadByIndex("Street", "street1");
            Assert.NotNull(retrievedValue2);
            Assert.Equal("name1", retrievedValue2.Name);
            Assert.Equal(10, retrievedValue2.Age);
            Assert.Equal("data1", retrievedValue2.Data);
            Assert.Equal("street1", retrievedValue2.Street);

            // find by md5 index
            var retrievedValue21 = cacheService.LoadByIndex("Md5Street", "street1".ToMD5());
            Assert.NotNull(retrievedValue21);
            Assert.Equal("name1", retrievedValue21.Name);
            Assert.Equal(10, retrievedValue21.Age);
            Assert.Equal("data1", retrievedValue21.Data);
            Assert.Equal("street1", retrievedValue21.Street);


            // test also the index by member expression
            var retrievedValue3 = cacheService.LoadByIndex(x => x.Age, "10");
            Assert.NotNull(retrievedValue3);
            Assert.Equal("name1", retrievedValue3.Name);
            Assert.Equal(10, retrievedValue3.Age);
            Assert.Equal("data1", retrievedValue3.Data);
            Assert.Equal("street1", retrievedValue3.Street);

            // delete cached value test
            cacheService.Delete(retrievedValue);
            var retrievedValue4 = cacheService.Load("name1");
            Assert.Null(retrievedValue4);
            // test also by idex
            var retrievedValue5 = cacheService.LoadByIndex("Street", "street1");
            Assert.Null(retrievedValue5);
            // load by member expression
            var retrievedValue6 = cacheService.LoadByIndex(x => x.Age, "10");
            Assert.Null(retrievedValue6);
        }

        // the same but async with different values of properties of tested entity
        [Fact]
        public async Task SetAndGetAsync_WithValidKeyAndValue_ShouldRetrieveCorrectValue()
        {
            // Arrange
            var cacheService = _fixture.GetService<IIndexedCacheService<TestChachedEntity>>(_testOutputHelper);
            Assert.NotNull(cacheService);
            // Act
            await cacheService.SaveAsync(new TestChachedEntity { Name = "name2", Age = 20, Data = "data2", Street = "street2" });
            var retrievedValue = await cacheService.LoadAsync("name2");

            // Assert
            Assert.NotNull(retrievedValue);
            Assert.Equal("name2", retrievedValue.Name);
            Assert.Equal(20, retrievedValue.Age);
            Assert.Equal("data2", retrievedValue.Data);
            Assert.Equal("street2", retrievedValue.Street);

            // test also the index
            var retrievedValue2 = await cacheService.LoadByIndexAsync("Street", "street2");
            Assert.NotNull(retrievedValue2);
            Assert.Equal("name2", retrievedValue2.Name);
            Assert.Equal(20, retrievedValue2.Age);
            Assert.Equal("data2", retrievedValue2.Data);
            Assert.Equal("street2", retrievedValue2.Street);

            // find by md5 index
            var retrievedValue21 = await cacheService.LoadByIndexAsync("Md5Street", "street2".ToMD5());
            Assert.NotNull(retrievedValue21);
            Assert.Equal("name2", retrievedValue21.Name);
            Assert.Equal(20, retrievedValue21.Age);
            Assert.Equal("data2", retrievedValue21.Data);
            Assert.Equal("street2", retrievedValue21.Street);

            // test also the index by member expression
            var retrievedValue3 = await cacheService.LoadByIndexAsync(x => x.Age, "20");
            Assert.NotNull(retrievedValue3);
            Assert.Equal("name2", retrievedValue3.Name);
            Assert.Equal(20, retrievedValue3.Age);
            Assert.Equal("data2", retrievedValue3.Data);
            Assert.Equal("street2", retrievedValue3.Street);

            // delete cached value test
            await cacheService.DeleteAsync(retrievedValue);
            var retrievedValue4 = await cacheService.LoadAsync("name2");
            Assert.Null(retrievedValue4);
            // test also by idex
            var retrievedValue5 = await cacheService.LoadByIndexAsync("Street", "street2");
            Assert.Null(retrievedValue5);
            // load by member expression
            var retrievedValue6 = await cacheService.LoadByIndexAsync(x => x.Age, "20");
            Assert.Null(retrievedValue6);
        }
    }
}