﻿using StackExchange.Redis;
using Xunit.Microsoft.DependencyInjection;
using Zerox.Cache.Backends;
using Zerox.Cache.Service;
using Zerox.Cahce.Service;

namespace Zerox.Cache.Fixture
{

    public class TestFixture : TestBedFixture
    {
        protected override void AddServices(IServiceCollection services, IConfiguration? configuration)
        {
            services
               //.AddTransient<ICacheBackend, DefaultRamBackEnd>()
               .AddSingleton<ICacheService, CacheService>()
               .AddSingleton(typeof(IIndexedCacheService<>), typeof(IndexedCahceService<>))
               .Configure<Options>(config => configuration?.GetSection("Options").Bind(config))
               .Configure<DefaultRamBackEndConfig>(config => configuration?.GetSection("DefaultRamBackEndConfig").Bind(config));

            services.AddSingleton<ICacheBackend, RedisBackend>();

            ConfigurationOptions options = new ConfigurationOptions
            {
                EndPoints = {
                    { "localhost", 7001 },
                    { "localhost", 7002 },
                    { "localhost", 7003 },
                    { "localhost", 7004 },
                    { "localhost", 7005 },
                    { "localhost", 7006 }
                },
                Password = "SUPER_SECRET_PASSWORD",
                AbortOnConnectFail = false,
            };

            services.AddSingleton(cfg =>
            {
                IConnectionMultiplexer multiplexer = ConnectionMultiplexer.Connect(options);
                return multiplexer.GetDatabase();
            });
        }

        protected override ValueTask DisposeAsyncCore()
            => new();

        protected override IEnumerable<TestAppSettings> GetTestAppSettings()
        {
            yield return new() { Filename = "appsettings.json", IsOptional = false };
        }
    }
}