## How to setup nuget repo in GitLab

- offitial doc GitLab: [Readme](https://docs.gitlab.com/ee/user/packages/nuget_repository/)
- create deploy token token, &lt;GitLab&gt;/repository : menu -> **settings** > **repository** > **deployTokens**
- then add nuget sources (this will add source in (%appdata%\Roaming\NuGet\NuGet.Config):

```
dotnet nuget add source https://gitlab.com/api/v4/projects/35215181/packages/nuget/index.json --name "ZeroX-3u-gitlab" --username "<deploy token user name>" --password "<deploy token>" --store-password-in-clear-text --valid-authentication-types basic

```
- nuget.config should be as this:

```xml
<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <packageRestore>
    <add key="enabled" value="True" />
    <add key="automatic" value="True" />
  </packageRestore>
  <bindingRedirects>
    <add key="skip" value="False" />
  </bindingRedirects>
  <packageManagement>
    <add key="format" value="0" />
    <add key="disabled" value="False" />
  </packageManagement>
  <packageSources>
    <add key="Package source" value="https://api.nuget.org/v3/index.json" />
    <add key="DevExpress 20.2 Local" value="C:\Program Files (x86)\DevExpress 20.2\Components\System\Components\Packages" />
    <add key="Microsoft Visual Studio Offline Packages" value="C:\Program Files (x86)\Microsoft SDKs\NuGetPackages\" />
    <add key="ZeroX-3u-gitlab" value="https://gitlab.com/api/v4/projects/35215181/packages/nuget/index.json" />
  </packageSources>
  <packageSourceCredentials>
    <ZeroX-3u-gitlab>
        <add key="Username" value="<token user name>" />
        <add key="ClearTextPassword" value="<password token value>" />
        <add key="ValidAuthenticationTypes" value="basic" />
      </ZeroX-3u-gitlab>
  </packageSourceCredentials>
</configuration>
```